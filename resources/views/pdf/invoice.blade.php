@extends('layout.invoice')

@section('content')

	<h1 class="text-center">INVOICE <i class="glyphicon glyphicon-yen"></i></h1>
	<div class="row">
		<div class="col-sm-6">
			<p>To : Instistute of New Khmer</p>
			<p>Attn : </p>
			<p>Address:.................... </p>
			<p>Tel : 012 587 395</p>
		</div>
		<div class="col-sm-6">
			<p>No. 000217-2015</p>
			<p>Date: 28-04-2015</p>
		</div>
	</div>
	<div class="hidden-print">
		<br/>
		<form class="form-inline">
			<div class="form-group description">
				<label class="sr-only" for="">Description</label>
				<input type="text" class="form-control require" id="description" placeholder="Description">
			</div>
			<div class="form-group quantity">
				<label class="sr-only" for="">Quantity</label>
				<input type="number" class="form-control require" id="quantity" placeholder="Quantity">
			</div>
			<div class="form-group unitprice">
				<label class="sr-only" for="">Unit Price</label>
				<input type="number" class="form-control require" id="unitPrice" placeholder="Unit Price">
			</div>
			<div class="form-group currency">
				<label class="sr-only" for="">Currency</label>
				<select class="form-control " name="" id="currency">
					<option value="riel">R</option>
					<option value="usd">USD</option>
				</select>
			</div>
			<button class="btn btn-danger" onclick="return false;" id="btnAddToTable">Add</button>
		</form>
		<br/>
	</div>
	<div class="invoice-container">
		<table class="table table-bordered" id="invoiceTable">
			<thead>
			<tr>
				<th width="7%">
					ល.រ
					<br/>
					No</th>
				<th>
					ការពិពណ៌នា
					<br/>
					Description</th>
				<th width="12%">
					បរិមាណ
					<br/>
					Quantity</th>
				<th width="15%">
					តំលៃរាយ
					<br/>
					Unit Price</th>
				<th width="18%">
					ចំនួនទឹកប្រាក់
					<br/>
					Amount</th>
			</tr>
			</thead>
			<tbody>

			</tbody>
			<tfoot>
			<tr>
				<td class="no-border"></td>
				<td class="no-border"></td>
				<td colspan="2" class="text-center">Total</th>
				<td class="text-center deposit"></td>
			</tr>
			<tr>
				<td class="no-border"></td>
				<td class="no-border"></td>
				<td colspan="2" class="text-center">Deposit</th>
				<td class="text-center deposit"></td>
			</tr>
			<tr>
				<td class="no-border"></td>
				<td class="no-border"></td>
				<td colspan="2" class="text-center">Grand Total</td>
				<td class="digit text-center grand-total"></td>
			</tr>
			</tfoot>
		</table>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<h4>INSTITUTE OF NEW KHMER</h4>
			<p> Signature and Stamp </p>
		</div>
		<div class="col-sm-6">
			<h4>HENG HENG COPY & PRINTING </h4>
			<p>Signature and Stamp</p>
			<br/>

			<h4>HENG KIMHIN</h4>

		</div>
	</div>
@endsection