@extends('layout.home')

@section('content')
    <div class="row">

        <div class="col-sm-12">
	        <h3 class="text-center">Invoices List</h3>
	        <div class="right">
		        <a href="/create-invoice" class="btn btn-default createInvoice"> Create new <i class="fa fa-plus"></i></a>
	        </div>

	        {{! $i=1}}
	        <table class="table table-bordered dataTable" style="margin-top: 15px">
		       <thead>
		       <tr>
			            <th>No</th>
			            <th>Customer</th>
			            <th>Invoice Number</th>
			            <th>Date</th>
			            <th>Amount</th>
			            <th>Deposit</th>
			            <th>Grand Total</th>
			            <th></th>
		            </tr>
		       </thead>
		       <tbody>

			       @foreach($invoices as $invoice)
				       <tr>
				       <td><?php echo sprintf('%02d', $i++)?></td>
				       <td>{{{$invoice->customer->name or ""}}}</td>
				       <td>{{$invoice->invoice_num}}</td>
				       <td>{{date('d-m-Y', $invoice->date)}}</td>
				       <td>{{$invoice->total}}</td>
				       <td>{{$invoice->deposit}}</td>
				       <td>{{$invoice->grand_total  }}</td>
				       <td class="text-center">
					       <a href="/edit-invoice/{{$invoice->id}}" class="text-center">
						       <i class="fa fa-edit fa-2x"></i>
					       </a>
				       </td>
			       </tr>
				@endforeach
		       </tbody>
	       </table>
        </div>
    </div>
@endsection
