@extends('layout.invoice')

@section('content')


	<div class="invoice text-center">
		<h2 class="title">វិក្ក័យប័ត្រ</h2>
		<h2 class="copper">Invoice</h2>
	</div>
    <div class="row invoice-header">
        <div class="col-xs-6">
	        <table class="customer">
		        <tbody>
		        <tr>
			        <td style="width: 60px;">To</td>
			        <td>: <input type="text" name="customer_name" value="{{{$invoice->customer->name or "" }}}"/>
				        <input type="hidden" name="customer_id" value="{{{$invoice->customer->id or 0 }}}"/>
			        </td>
		        </tr>
		        <tr>
			        <td>Attn</td>
			        <td>: <input type="text" name="customer_attn" value="{{{$invoice->customer->attn or "" }}}"/> </td>
		        </tr>
		        <tr>
			        <td>Address</td>
			        <td>: <input type="text" name="customer_address" value="{{{$invoice->customer->address or "" }}}"/></td>
		        </tr>
		        <tr>
			        <td>Tel </td>
			        <td>: <input type="text" name="customer_tel" value="{{{$invoice->customer->tel or "" }}}"/></td>
		        </tr>
		        </tbody>
	        </table>
        </div>
	    <div class="col-xs-2 col-xs-offset-4">
		    <table class="invoice">
			    <tbody>
			    <tr>
				    <td class="" style="width: 8%">No :</td>
				    <td style="width: 30%"><input class="invoice_id text-right" type="text" value="{{$invoice->invoice_num}}" name="invoice_number" disabled/>
					    <input type="hidden" name="invoice_id" value="{{$invoice->id}}" />
				    </td>
			    </tr>
			    <tr>
				    <td class="" style="width: 8%">Date :</td>
				    <td style="width: 30%"><input type="text" class="datepicker text-right" placeholder="Date" name="invoice_date" value="{{date('d-m-Y', $invoice->date)}}" /></td>
			    </tr>
			    </tbody>
		    </table>
        </div>
    </div>
    <div class="hidden-print">
        <br/>
        <form class="form-inline">
            <div class="form-group description">
                <label class="sr-only" for="">Description</label>
                <input type="text" class="form-control require" id="description" placeholder="Description">
            </div>
            <div class="form-group quantity">
                <label class="sr-only" for="">Quantity</label>
                <input type="number" class="form-control require" id="quantity" placeholder="Quantity">
            </div>
            <div class="form-group unitprice">
                <label class="sr-only" for="">Unit Price</label>
                <input type="number" class="form-control require" id="unitPrice" placeholder="Unit Price">
            </div>
            <div class="form-group currency">
                <label class="sr-only" for="">Currency</label>
                <select class="form-control " name="" id="currency">
                    <option value="riel">R</option>
                    <option value="usd">USD</option>
                </select>
            </div>
            <button class="btn btn-danger" onclick="return false;" id="btnAddToTable">Add</button>
        </form>
        <br/>
    </div>
    <div class="invoice-container">
        <table class="table table-bordered" id="invoiceTable">
            <thead>
            <tr>
                <th width="7%">
                    ល.រ
                    <br/>
                    No</th>
                <th>
                    ការពិពណ៌នា
                    <br/>
                    Description</th>
                <th width="12%">
                    បរិមាណ
                    <br/>
                    Quantity</th>
                <th width="15%">
                    តំលៃរាយ
                    <br/>
                    Unit Price</th>
                <th width="18%">
                    ចំនួនទឹកប្រាក់
                    <br/>
                    Amount</th>
            </tr>
            </thead>
            <tbody>
            {{! $i=1 }}
            @foreach($invoice->invoiceItem as $item)
            <tr>
	            <td class="numbering"><?php echo sprintf('%02d', $i++)?></td>
	            <td class="text-left"><input type=\"text\" value="{{$item->title}}" /></td>
	            <td class="digit qty"><input class="text-center" type=\"text\" value="{{$item->quantity}}" /></td>
	            <td class="digit price"><input class="text-right" type=\"text\" value="{{$item->unitPrice}}" /></td>
	            <td class="digit text-right amount-total {{$item->currency_id == 1 ?  "R":"USD"}}">{{$item->amount}} <i data-id="{{$item->id}}" class='glyphicon-trash glyphicon remove-row hidden-print'></i></td>
            </tr>
            @endforeach
            </tbody>
	            <tfoot>
                <tr>
                    <td class="no-border"></td>
                    <td class="no-border"></td>
                    <td colspan="2" class="text-right"><strong>Total :&nbsp;&nbsp;</strong></th>
                    <td class="text-right total"></td>
                </tr>
                <tr>
                    <td class="no-border"></td>
                    <td class="no-border"></td>
                    <td colspan="2" class="text-right"><strong>Deposit :&nbsp;&nbsp;</strong></td>
                    <td class="deposit"><div id="deposit" class="text-right" contenteditable="true"> &nbsp;</div>
                </tr>
                <tr>
                    <td class="no-border"></td>
                    <td class="no-border"></td>
                    <td colspan="2" class="text-right "><strong>Grand Total :&nbsp;&nbsp;</strong></td>
                    <td class="digit text-right grand-total"></td>
                </tr>
            </tfoot>
        </table>

	    <br/>
    <div class="row text-center">
        <div class="col-xs-6">
            <h4 class="copper">Customer Authorized</h4>
	        <p class="copperlg"> Signature and Stamp </p>
            <br/>
            <br/>
	        <br/>
            <br/>
	        <h4 class="copper" id="footerCustomer">{{{ $invoice->customer->name or "" }}}</h4>
        </div>
        <div class="col-xs-6 ">
            <h4 class="copper">Heng Heng Digital PRINTING </h4>
            <p class="copperlg">Signature and Stamp</p>
            <br/>
            <br/>
            <br/>
	        <br/>
	        <h4 class="copper" contenteditable="true">{{env("SHOP_OWNER")}}</h4>

        </div>
    </div>
    </div>
@endsection