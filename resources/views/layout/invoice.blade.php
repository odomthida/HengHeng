<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Heng Heng - {{{ $title or 'Default' }}}</title>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

    <link href='http://fonts.googleapis.com/css?family=Battambang|Moul' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.css')}}"/>

    <link rel="stylesheet" href="{{asset('css/app.css')}}"/>

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{asset('img/icon/apple-touch-icon.png')}}" >
    <link rel="icon" href="{{asset('img/icon/favicon.ico')}}">
</head>

<body>
    <div class="container" id="mainContain">
        <div class="row header_image" >
            <div class="col-sm-12">
                <img src="{{asset('img/HengHeng.jpg')}}" alt="Heng Heng"/>
	            <div class="" style="font-family: 'Battambang'; font-size:11px">
		            <br/>
		            <p>ឣាស័យដ្ឋាន​៖​ ផ្ទះលេខ ១២​ ផ្លូវ ២៧១ សង្កាត់ទឹកថ្លា​ ខណ្ឌសែនសុខ រាជធានីភ្នំពេញ ក្បែរសាកលវិទ្យាល័យ​មេគ្គងកម្ពុជា</p>
		            <p class="right" >សូមឣរគុណដែលបាន​ប្រើប្រាស់​សេវាកម្មយើងខ្ញុំ</p>
	            </div>
            </div>
        </div>
        @yield('content')
    </div>

    <script src="{{asset('javascripts/jquery-2.1.4.min.js')}}"> </script>
    <script src="{{asset('javascripts/moment.js')}}"> </script>
    <script src="{{asset('javascripts/bootstrap.min.js')}}"> </script>
    <script src="{{asset('javascripts/bootstrap-datetimepicker.min.js')}}"> </script>
    <script src="{{asset('javascripts/numeral.js')}}"></script>

    <script src="{{asset('javascripts/invoice.js')}}"></script>
    <script src="{{asset('javascripts/addional.js')}}"></script>
</body>
</html>