<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Heng Heng - {{{ $title or 'Home' }}}</title>
    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('css/app.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/jquery.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}"/>

</head>

<body>
<div class="container">
	<div class="row header_image" >

			<img src="{{asset('img/HengHeng.jpg')}}" width="100%" alt="Heng Heng"/>

	</div>
    @yield('content')
</div>
<script src="{{asset('javascripts/jquery-2.1.4.min.js')}}"> </script>
<script src="{{asset('javascripts/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('javascripts/bootstrap.min.js')}}"> </script>
<script>
    $('.dataTable').dataTable();

</script>
</body>
</html>