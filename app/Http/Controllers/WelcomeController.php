<?php namespace App\Http\Controllers;



use App\Customer;
use App\Invoice;
use App\InvoiceItem;
use Illuminate\Http\Request;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index() {
        $data['title'] = "Home";
		$data['invoices'] = Invoice::all();
		return view('welcome.index', $data);
	}

    public function invoice() {
        $data['title'] = "Invoice";
        return view('welcome.invoice', $data);
    }

	public function  getCreateInvoice($id=null) {
		if (is_null($id)) {
			$invoice = new Invoice();
			$invoice->invoice_num = '';
			$invoice->date = time();
			$invoice->save();
			$invoice->invoice_num = $invoice->getLastNumber();
			$invoice->save();
			return redirect('/create-invoice/'.$invoice->id);
		} else {
			$invoice = Invoice::find($id);

			if (is_null($invoice)) return redirect('/');
			$data['invoice'] = $invoice;

		}
		$data['title'] = "Create New";
		return view('welcome.create-invoice', $data);
	}

	public function getEditInvoice($id = null) {
		if (is_null($id)) return redirect('/create-invoice');
		$invoice  = Invoice::find($id);
		if (count($invoice)) {
			$data['title'] = "Edit Invoice";
			$data['invoice'] = $invoice;

			return view('welcome.create-invoice', $data);
		} else {
			dd("Invoice Incorrect!! ");
		}
	}

	public function postUpdateOrCreateCustomer(Request $request) {
		$all = $request->all();
		if ($all['id']) {
			$customer= Customer::find($all['id']);
		} else {
			$customer = new Customer();
		}
		$customer->name = $all['name'];
		$customer->attn = $all['attn'];
		$customer->address = $all['address'];
		$customer->tel = $all['tel'];
		$customer->save();
		$invoice = Invoice::find($all['id_invoice']);
		$invoice->customer_id = $customer->id;
		$invoice->save();
		return $customer->id;
	}

	public function postUpdateInvoice(Request $request) {
		$all = $request->all();
		if (isset($all['id'])) {
			$invoice = Invoice::find($all['id']);
			if (! is_null($invoice)) {
				$date = date_create_from_format('d-m-Y',$all['invoice_date']);
				$invoice->date = $date->getTimestamp();
				$invoice->save();
			}

		}
	}


	public function postAddInvoiceItem( Request $request) {
		$all = $request->all();
		$invoiceItem = new InvoiceItem();
		$invoiceItem->title = $all['title'];
		$invoiceItem->quantity = $all['quantity'];
		$invoiceItem->unitPrice = $all['price'];
		$invoiceItem->amount = $all['price']*$all['quantity'];
		$invoiceItem->invoice_id = $all['invoice_id'];
		$invoiceItem->currency_id = $all['currency']=='R'?1:2;
		$invoiceItem->save();
		return $invoiceItem->id;
	}

	public function postDeleteItem( Request $request) {
		$all = $request->all();
		$invoiceItem = InvoiceItem::find($all['id']);
		if (count($invoiceItem)) $invoiceItem->delete();


	}

	public function postUpdateDeposit(Request $request) {
		$all = $request->all();

		if (isset($all['id_invoice'])) {
			$invoice = Invoice::find($all['id_invoice']);

			if (! is_null($invoice)) {
				$invoice->total = $all['total'];
				$invoice->deposit = $all['deposit'];
				$invoice->grand_total = $all['grand_total'];
				$invoice->save();
				return 'done';
			}

		}
	}

	public function postUpdateInvoiceitem(Request $request) {
		$all = $request->all();

		if (isset($all['id'])) {
			$invoiceitem = InvoiceItem::find($all['id']);
			if (! is_null($invoiceitem)) {
				$invoiceitem->quantity = $all['quantity'];
				$invoiceitem->unitPrice = $all['price'];
				$invoiceitem->amount = $all['quantity'] * $all['price'];
				$invoiceitem->save();
				return 'done';
			}

		}
	}




}
