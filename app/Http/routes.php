<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');
Route::get('invoice', 'WelcomeController@invoice');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	'/' => 'WelcomeController'
]);

Route::get('pdf', function() {
	$data = array('data'=>"Hello World");
	$pdf = PDF::loadView('pdf.invoice', $data);
	return $pdf->download('invoice.pdf');
});
