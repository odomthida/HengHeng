<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model {

	use SoftDeletes;

	public function getLastNumber () {
		$data = $this->orderBy('id', 'desc')->take(1)->get();
		if  ( count($data)) {
			return sprintf('%06d', $data[0]->id).env("INV_FORM");
		}
	}

	public function invoiceItem() {
		return $this->hasMany('App\InvoiceItem', 'invoice_id');
	}


	public function customer() {
		return $this->belongsTo ('App\Customer', 'customer_id');
	}

}
