/*
 *
 *
 * Additional Function for using
 *
 * */
var hasRequireClass = function (array) {
    var t=true;
    for (var i=0; i<array.length; i++) {
        if ($(array[i]).hasClass('require')) {
            $(array[i]).addClass('red');
            t=false;
            break;
        }
    }
    return t;
}

var updateNumber =  function () {
    num = $('.numbering').length;
    for(var i =0; i<num; i++ ) {
        $($('.numbering')[i]).text(twoDigi(i+1));
    }

}

var twoDigi = function(number) {
    number +="";
    if (number.length < 2) {
        return "0" + number;
    }
    return number;
}

var addToTable = function  (des, quantity, price, currency, id) {
    var row = "<tr>" +
        "<td class=\"numbering \"></td>" +
        "<td class=\"text-left \"> <input type=\"text\" value=\" "+des+" \"/></td>" +
        "<td class=\"digit qty\"> <input class=\"text-center\" type=\"text\" value=\" "+numeral(quantity).format('0,0')+" \"/></td>" +
        "<td class=\"digit price \"><input class=\"text-right\" type=\"text\" value=\" "+ formatMoney(price, "")+" \"/></td>" +
        "<td class=\"digit amount-total text-right "+ currency +"\">"+ formatMoney(quantity * price, currency) + " <i data-id=\"id\" class='glyphicon-trash glyphicon remove-row hidden-print'></i></td>" +
        "</tr>"
    $("#invoiceTable tbody").append(row);
}

var addInvoiceItem = function (des,quantity, price, currency, invoice_id) {
    console.log('add invoice item');
    $.ajax({
        'url': '/add-invoice-item',
        'method':'POST',
        'data': {
            'invoice_id': invoice_id,
            'title': des,
            'price': price,
            'quantity': quantity,
            'currency': currency
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function(id){
        addToTable(des, quantity,  price, currency, id);
        setTimeout(total(), 1000);
        clear();
    }).success(function(data){
        console.log(data);
    }).fail(function(data){
        console.log(data);
    });

}

var clear = function (){
    $("#description").val('');
    $('#quantity').val('');
    $('#unitPrice').val('');
};

var total = function () {
    amount= $('.amount-total');
    grandTotalR = 0;
    grandTotalUSD = 0;

    for ( var i=0; i<amount.length; i++ ) {
        var amt = $(amount[i]).text();
        if ( $(amount[i]).hasClass("R")) {
            grandTotalR += numeral().unformat(amt) * 1;
        } else {
            grandTotalUSD += numeral().unformat(amt) * 1;
        }

    }
    totalR = numeral(grandTotalR).format('0,0[.]00') + "R";
    totalUSD = numeral(grandTotalUSD).format('0,0[.]00') + "USD";

    gTotalR = numeral(grandTotalR - numeral().unformat($('.depositR').text())*1).format('0,0[.]00') + "R";
    gTotalUSD = numeral(grandTotalUSD - numeral().unformat($('.depositUSD').text())*1).format('0,0[.]00') + "USD";
    totalPrice = totalR+ " + "  + totalUSD;

    gTotal =  gTotalR + "+" + gTotalUSD;
    if  (grandTotalR == 0) {
        totalPrice = totalUSD;
        gTotal = gTotalUSD

    } else if (grandTotalUSD == 0){
        totalPrice = totalR;
        gTotal = gTotalR;
    }

    $('.total').text(totalPrice);
    $('.grand-total').text(gTotal);
    updateNumber();
    updateDeposit();
}

var updateInvoice = function() {
    $.ajax({
        'url': '/update-invoice',
        'method':'POST',
        'data': {
            'id': $('input[name="invoice_id"]').val(),
            'invoice_date': $('input[name="invoice_date"]').val()
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function(){
        console.log("done");
    }).success(function(data){
        console.log(data);
    }).fail(function(data){
        console.log(data);
    });
}

var updateCustomer = function() {
    $.ajax({
        'url': '/update-or-create-customer',
        'method':'POST',
        'data': {
            'id': $('input[name="customer_id"]').val(),
            'id_invoice': $('input[name="invoice_id"]').val(),
            'name': $('input[name="customer_name"]').val(),
            'attn': $('input[name="customer_attn"]').val(),
            'address': $('input[name="customer_address"]').val(),
            'tel': $('input[name="customer_tel"]').val()
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function(response){
        $('input[name="customer_id"]').val(response)
    }).success(function(data){
        console.log(data);
    }).fail(function(data){
        console.log(data);
    });
}

var updateIvoiceItem = function(id, quanity, price) {
    $.ajax({
        'url': '/update-invoiceitem',
        'method':'POST',
        'data': {
            'id': id,
            'quantity': quanity,
            'price': price
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function(response){
        $('input[name="customer_id"]').val(response)
    }).success(function(data){
        console.log(data);
    }).fail(function(data){
        console.log(data);
    });
}

var updateDeposit = function () {
    $.ajax({
        'url': '/update-deposit',
        'method':'POST',
        'data': {
            'id_invoice': $('input[name="invoice_id"]').val(),
            'total': $('.total').text(),
            'deposit': $('#deposit').text(),
            'grand_total': $('.grand-total').text()
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function(response){
        console.log(response)
    })
}

var formatMoney = function (ammount, currency) {
    return numeral(ammount).format('0,0[.]00')+ currency
}