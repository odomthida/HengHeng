$(document).ready(function() {
    $("#btnAddToTable").click(function(){
        var des =   $("#description");
        var quantity =  $('#quantity');
        var uPrice =   $('#unitPrice');
        var  array = ["#description", "#quantity", "#unitPrice"];
        if (hasRequireClass(array)) {
            currency = $('.currency option:selected').text();

            addInvoiceItem(des.val(),quantity.val(),uPrice.val(), currency, $('input[name="invoice_id"]').val());

            for (var i=0; i<array.length; i++) {
                $(array[i]).addClass('require');
            }
        }
    });
    $('table').on('click','.remove-row', function () {
        tr = $(this).parents('tr');
        $.ajax({
            'url': '/delete-item',
            'method':'POST',
            'data': {
                'id': $(this).data('id')
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function(response) {

        }).success(function(data){

            $(tr[0]).fadeOut('slow');
            setTimeout(function(){
                $(tr[0]).remove();
                updateNumber();
                total();
            }, 500)
        }).fail(function(data){
            console.log(data);
        });


    });

    $("#description, #quantity, #unitPrice").focusout(function(){
        if ($(this).val().length == 0) {
            $(this).addClass("require red");
        } else {
            $(this).removeClass('require red');
        }
    });

    $('body').on("click", "td i.glyphicon-edit", function(){
        console.log(   $(this).parent('td').text());
    });

    $datepicker = $(".datepicker");
    if ($datepicker.length) {

        option = {
            format: "DD-MM-YYYY",
            showTodayButton: true
        };
        $datepicker.datetimepicker(option);
    }

    $tableInvoice = $('table.invoice');

    if ($tableInvoice.length) {
        $invoiceDate = $tableInvoice.find('input[name="invoice_date"]');
        $invoiceDate.focusout(function() {
            updateInvoice();
        });

        $tableCustomer = $('table.customer');
        $tableCustomer.find('input').change(function(){
            updateCustomer();

            $('#footerCustomer').text($('input[name="customer_name"]').val());
        });

        //Deposit
        $deposit=$('#deposit ');
        $deposit.focusout(function () {
            deposit = $(this).text().trim();
            currency = $('#currency option:selected').text();
            if (currency == "R") {
                $(this).addClass("depositR");
                $(this).removeClass("depositUSD");
            } else {
                $(this).addClass("depositUSD");
                $(this).removeClass("depositR");
            }
            $(this).text(formatMoney(deposit, currency));
            total();
        });

        $deposit.focusin(function(){
            $(this).text("");
        });

        amount = $('.amount-total');
        for ( var i=0; i<amount.length; i++ ) {
            var amt = $(amount[i]).text();
            var itage = $(amount[i]).find('i')[0];

            if ( $(amount[i]).hasClass("R")) {
                $(amount[i]).html(formatMoney(amt, "R")).append(itage);

            } else {
                $(amount[i]).html(formatMoney(amt, "USD")).append(itage);
            }
        }
        total();
    }
    $('.digit').change(function(){
        amount = $(this).siblings('.digit');
        v1 = $(this).children('input').val();
        v2 = $(amount[0]).children("input").val();
        id = $(amount[1]).children('i').data('id')
        var del = '<i data-id="'+id+'" class="glyphicon-trash glyphicon remove-row hidden-print"></i>';
        if ($(amount[1]).hasClass("R")) {
            $(amount[1]).text(formatMoney(v1 * v2, "R"));
        } else {
            $(amount[1]).text(formatMoney(v1 * v2, "USD") );
        }
        if ($(amount[0]).hasClass('qty')) {
            quantity = v2;
            price = v1;
        } else {
            quantity = v1;
            price = v2;
        }
        updateIvoiceItem(id, quantity, price);
        $(amount[1]).append(del);
        total();
    });
});
/* END OF DOCUMENT READY*/
